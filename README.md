# Trove Downloader

This (command line) program is to download all the items in the Humble Bundle Trove.  
So when this months monthly was revealed (May 2019) I didnt like it so I tried to cancel whereupon I realised that I would loose access to the Trove.

Now instead of doing the normal thing and download them myself I decided to delve into a new language (rust) to automate the process.  
I have not failed.

So this is my frst project in rough, started less than a week ago. Some of the code is rough af, my friend and mentor couldnt get past the first 10 lines or so before stuff was lobbed my way.  
This also gave me a chance to explore ci a bit, the binaries were built by gitlab on my behalf (slight smug about that).

I may work on the UI of it later, making it more user friendly, too tired to do it right now.

*****
# How it works

* Download (or compile) the files listed below.
* Firstrun you will have to provide at least an api key (Details on how to get it below)
* The program will download trove.json from the official api.
* it will then go through trove.json downloading all the files, it will mark the completed ones with the md5 hash of the file.
* When the program is run again (without arguments) it will use the arguments saved from the previous time. It will also skip any downloaded files.
* It will go through trove.json and if the new md5 hash is different from the previous it will download the new one.
* Running the program and supplying it with new arguments will overwrite the old ones.

****
# Installation.

If you have rust then you can compile it yourself.  
If you dont then the built files are below.  
If you are worried about the security of these they were built by gitlab on my behalf from the code I uploaded, here [have a look.](https://gitlab.com/silver_rust/trove_downloader/pipelines)

* [Linux](https://gitlab.com/silver_rust/trove_downloader/-/jobs/artifacts/master/raw/target/x86_64-unknown-linux-musl/release/trove_downloader?job=x86_64-unknown-linux-musl)
* [Windows](https://gitlab.com/silver_rust/trove_downloader/-/jobs/artifacts/master/raw/target/x86_64-pc-windows-gnu/release/trove_downloader.exe?job=x86_64-pc-windows-gnu)

Put the files where-ever you want and move onto the next part.

****

# Useage

## Getting the Key

You will need to get an api key, [Instructions here.](https://github.com/talonius/hb-downloader/wiki/Using-Session-Information-From-Windows-For-hb-downloader)

## Commands
### Run

This is the main command, it has 3 parameters.  
First run only key is required.  
After the firstrun no arguments are required

* Key - from the instructions above
* Location - the path you want items downloaded to - defaults to same folder the executable is in.
* Platform - all/windows/mac/linux/asmjs/unitywasm - defaults to windows

I highly recommend putting the api key and location in quotes.

### Update

This fetches a fresh executable from the repo to replace the current one.

## Usage

### Firstrun

#### Common

Go to where the executable is and open up Powershell/cmd (windows) or bash (linux)

#### Windows
``./trove_downloader.exe run --key "<API KEY HERE>" --platform "<all/windows/mac/linux/asmjs/unitywasm>" --location "C:/Games/Trove"``

#### Linux
``./trove_downloader run --key "<API KEY HERE>" --platform "<all/windows/mac/linux/asmjs/unitywasm>" --location "/home/user/Games/Trove"``

### After firstrun

The arguments from the firstrun are saved so its simply:

#### Windows
``./trove_downloader.exe run``

#### Linux
``./trove_downloader run``

### Updating arguments
Simply pass an argument to update it

#### Windows
``./trove_downloader.exe run --key "<New KEY HERE>"``

#### Linux
``./trove_downloader run --key "<New API KEY HERE>"``

### Updating the program

#### Windows
``./trove_downloader.exe update``

#### Linux
``./trove_downloader update``

#### Misc
If it does not auto detect your system then run:  
``./trove_downloader.exe update --platform "windows"``  
or  
``./trove_downloader update --platform "linux"``  

****

# Download sizes and times
I have a 5MB/s internet connection at home.  
So ehre are the times and sizes I got (May 2019)

| System | Time | Size |
|---|---|---|
| Windows | 5 Hrs | 67.4 GB |
| Mac | 3.5 Hrs | 44.1 GB |
| Linux | 3 Hrs | 39.6 GB |

****

# Improving this

I will be working on improving this in the coming while, it is my first rust program and my first compiled program as well.  
While it is functional in its current state I do plan to refactor it with better patterns in my code (and so my friend dosent murder me for having this mess...).  
I also plan to look into compiling it for mac since hb supports mac. Tried to get it working before this but didnt succeed.

If you have any ideas on how to improve this, submit a PR or ping me on discord (Silver#5563)
